package com.example.personaltracking.Entity;

import java.util.Date;

public class Servicio {
    private String pkIdService;
    private String serviceName;
    private String serviceType;
    private String serviceDescription;
    private String serviceCreation; //TODO:date para cuando registre tipo fecha
    private String serviceEquipment;
    private String serviceStart;//TODO:date para cuando registre tipo fecha
    private String serviceEnd;//TODO:date para cuando registre tipo fecha
    private String idClient;
    private String clientContract;

    public Servicio(String pkIdService, String serviceName, String serviceType, String serviceDescription,
                    String serviceCreation,String serviceEquipment,String serviceStart, String serviceEnd,
                    String idClient, String clientContract){
        this.pkIdService=pkIdService;
        this.serviceName=serviceName;
        this.serviceType=serviceType;
        this.serviceDescription=serviceDescription;
        this.serviceCreation=serviceCreation;
        this.serviceEquipment=serviceEquipment;
        this.serviceStart=serviceStart;
        this.serviceEnd=serviceEnd;
        this.idClient=idClient;
        this.clientContract=clientContract;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getServiceEnd() {
        return serviceEnd;
    }

    public void setServiceEnd(String serviceEnd) {
        this.serviceEnd = serviceEnd;
    }

    public String getServiceStart() {
        return serviceStart;
    }

    public void setServiceStart(String serviceStart) {
        this.serviceStart = serviceStart;
    }


    public String getServiceCreation() {
        return serviceCreation;
    }

    public void setServiceCreation(String serviceCreation) {
        this.serviceCreation = serviceCreation;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getClientContract() {
        return clientContract;
    }

    public void setClientContract(String clientContract) {
        this.clientContract = clientContract;
    }

    public String getPkIdService() {
        return pkIdService;
    }

    public void setPkIdService(String pkIdService) {
        this.pkIdService = pkIdService;
    }

    public String getServiceEquipment() {
        return serviceEquipment;
    }

    public void setServiceEquipment(String serviceEquipment) {
        this.serviceEquipment = serviceEquipment;
    }
}
