package com.example.personaltracking.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.personaltracking.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class MyFirebaseInstanceService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("LOGALF===>",remoteMessage.getFrom());

        ShowNotificacion(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());



    }

    private void ShowNotificacion(String title, String body) {
        NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID="com.example.personaltracking.mensaje";

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel nch=new NotificationChannel(NOTIFICATION_CHANNEL_ID,"NOTIFICATION",NotificationManager.IMPORTANCE_DEFAULT);
            nch.setDescription("EDMT Channel");
            nch.enableLights(true);
            nch.setLightColor(Color.BLUE);
            nch.setVibrationPattern(new long[]{0,1000,500,1000});
            nm.createNotificationChannel(nch);
        }

        NotificationCompat.Builder nb = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);
        nb.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_notification)
                .setContentText(body)
                .setContentInfo("Info");

        nm.notify(new Random().nextInt(),nb.build());

    }

    /**TODO: AYUDA FIREBASE
     if (remoteMessage.getData().size() > 0) {
     Log.d(TAG, "Message data payload: " + remoteMessage.getData());
         if (true) {
            // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
            scheduleJob();
        } else {
            // Handle message within 10 seconds
            handleNow();
        }
     }
     if (remoteMessage.getNotification() != null) {
     Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
     }
      **/
}
