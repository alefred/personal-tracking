package com.example.personaltracking.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.personaltracking.tools.DAOException;
import com.example.personaltracking.Entity.Sugerencia;
import com.example.personaltracking.tools.DbHelper;

import java.util.ArrayList;

public class SugerenciaDAO {
    private DbHelper _dbHelper;

    public SugerenciaDAO(Context c) {
        _dbHelper = new DbHelper(c);
    }

    public void insertar(String titulo, String descripcion) throws DAOException {
        Log.i("SugerenciaDAO", "insertar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        try {
            String[] args = new String[]{titulo, descripcion};
            db.execSQL("INSERT INTO genero(titulo, descripcion) VALUES(?,?)", args);
            Log.i("SugerenciaDAO", "Se insertó");
        } catch (Exception e) {
            throw new DAOException("SugerenciaDAO: Error al insertar: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public Sugerencia obtener() throws DAOException {
        Log.i("SugerenciaDAO", "obtener()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        Sugerencia modelo = new Sugerencia();
        try {
            Cursor c = db.rawQuery("select id, titulo, descripcion from genero", null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int id = c.getInt(c.getColumnIndex("id"));
                    String titulo = c.getString(c.getColumnIndex("titulo"));
                    String descripcion = c.getString(c.getColumnIndex("descripcion"));

                    modelo.setId(id);
                    modelo.setTitulo(titulo);
                    modelo.setDescripcion(descripcion);

                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("SugerenciaDAO: Error al obtener: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return modelo;
    }

    public ArrayList<Sugerencia> buscar(String criterio) throws DAOException {
        Log.i("GeneroMusicalDAO", "buscar()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        ArrayList<Sugerencia> lista = new ArrayList<Sugerencia>();
        try {
            Cursor c = db.rawQuery("select id, titulo, descripcion from genero where titulo like '%"+criterio+"%' or descripcion like '%"+criterio+"%'", null);

            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int id = c.getInt(c.getColumnIndex("id"));
                    String titulo = c.getString(c.getColumnIndex("titulo"));
                    String descripcion = c.getString(c.getColumnIndex("descripcion"));

                    Sugerencia modelo = new Sugerencia();
                    modelo.setId(id);
                    modelo.setTitulo(titulo);
                    modelo.setDescripcion(descripcion);

                    lista.add(modelo);
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("SugerenciaDAO: Error al obtener: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return lista;
    }

    public void eliminar(int id) throws DAOException {
        Log.i("SugerenciaDAO", "eliminar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            String[] args = new String[]{String.valueOf(id)};
            db.execSQL("DELETE FROM genero WHERE id=?", args);
        } catch (Exception e) {
            throw new DAOException("SugerenciaDAO: Error al eliminar: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public void eliminarTodos() throws DAOException {
        Log.i("SugerenciaDAO", "eliminarTodos()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            db.execSQL("DELETE FROM genero");
        } catch (Exception e) {
            throw new DAOException("SugerenciaDAO: Error al eliminar todos: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

}
