package com.example.personaltracking.tools;

public class DAOException extends Exception  {
    public DAOException(String detailMessage) {
        super(detailMessage);
    }
}
