package com.example.personaltracking;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.personaltracking.Entity.GastoMarcador;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback {

    GoogleMap nMap;
    SupportMapFragment mapFragment;
    private List<GastoMarcador> marcadores=new ArrayList<>();

    public MapsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vistaRaiz = inflater.inflate(R.layout.fragment_maps, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if(mapFragment==null){
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            mapFragment=SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
         mapFragment.getMapAsync(this);
        return vistaRaiz;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        nMap=googleMap;
        LatLng enfoquecam = new LatLng (-12.197477,-77.0094861);
        nMap.moveCamera(CameraUpdateFactory.newLatLng(enfoquecam));
        nMap.animateCamera(CameraUpdateFactory.newLatLng(enfoquecam));
        nMap.animateCamera(CameraUpdateFactory.newLatLngZoom(enfoquecam, 13.0f));
        prepararDatos();
        for(GastoMarcador marcador : marcadores) {
            nMap.addMarker(agregarMarcador(marcador.getTitulo(),
                    marcador.getSnippet(),marcador.getLat(),marcador.getLng()));
        }
    }

    public MarkerOptions agregarMarcador(String titulo, String snippet, Double lat, Double lng){

        LatLng latLng = new LatLng(lat,lng);
        MarkerOptions marcador = new MarkerOptions();
        marcador.position(latLng);
        marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        marcador.title(titulo);
        marcador.snippet(snippet);
        Log.i("LOGALF====>","latlng:"+lat+lng );

        return marcador;
    }

    private void prepararDatos(){
        GastoMarcador marcador = new GastoMarcador("Mayorca","la pola robledo",6.285588,-75.599480);
        marcadores.add(marcador);
        marcador = new GastoMarcador("UPC","Clases de DPM",-12.197477,-77.0094861);
        marcadores.add(marcador);
        marcador = new GastoMarcador("Plaza Vea","Almuerzo del Miercoles",-12.1974716,-77.0095451);
        marcadores.add(marcador);
        marcador = new GastoMarcador("BCP Sede CHO","Cliente",-12.1794684,-77.0179843);
        marcadores.add(marcador);
        marcador = new GastoMarcador("Almuerzo Cimpleaños","El Encuentro Otani",-12.1825539,-76.9982171);
        marcadores.add(marcador);
    }


}
