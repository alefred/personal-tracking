package com.example.personaltracking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.personaltracking.Entity.Sugerencia;
import com.example.personaltracking.Adapter.SugerenciaDAO;
import com.example.personaltracking.tools.DAOException;
import com.microsoft.cognitiveservices.speech.ResultReason;
import com.microsoft.cognitiveservices.speech.SpeechConfig;
import com.microsoft.cognitiveservices.speech.SpeechRecognitionResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Future;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.RECORD_AUDIO;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ServiceFragment.OnFragmentInteractionListener,
        SharedFragment.OnFragmentInteractionListener,
        ListServiceFragment.OnFragmentInteractionListener,
        SugerenciaFragment.OnFragmentInteractionListener,
        ListSugerenciaFragment.OnFragmentInteractionListener
        {
            FragmentManager fragmentManager = getSupportFragmentManager();

            //Datos azure
            private static String speechSubscriptionKey = "0fd5ccff4f47420486c6c0be8368dde6";
            private static String serviceRegion = "westus2";
            private static String LanguageRecognition = "es-ES";

            public static final MediaType JSON
                    = MediaType.get("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            String url="http://wspersonaltrackingservice.azurewebsites.net/api/service";

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_navigation);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);

                //init View:
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();

                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setNavigationItemSelectedListener(this);

                int requestCode = 5; // unique code for the permission request
                ActivityCompat.requestPermissions(NavigationActivity.this, new String[]{RECORD_AUDIO, INTERNET}, requestCode);
            }

            @Override
            public void onBackPressed() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    //super.onBackPressed();
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new ServiceFragment()).commit();
                }
            }

            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.navigation, menu);
                fragmentManager.beginTransaction().replace(R.id.contenedor, new ServiceFragment()).commit();
                return true;
            }

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                // Handle action bar item clicks here. The action bar will
                // automatically handle clicks on the Home/Up button, so long
                // as you specify a parent activity in AndroidManifest.xml.
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_settings) {
                    Intent login = new Intent(this,LoginActivity.class);
                    startActivity(login);
                }
                    return super.onOptionsItemSelected(item);
            }

            @SuppressWarnings("StatementWithEmptyBody")
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.nav_newgasto) {
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new ServiceFragment()).commit();
                } else if (id == R.id.nav_historialgasto) {
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new ListServiceFragment()).commit();
                } else if (id == R.id.nav_compartidogasto) {
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new ImageFragment()).commit();
                } else if (id == R.id.nav_ubicaciongasto) {
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new MapsFragment()).commit();
                } else if (id == R.id.nav_contact) {
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new SugerenciaFragment()).commit();
                } else if (id == R.id.nav_about) {
                    fragmentManager.beginTransaction().replace(R.id.contenedor, new ListSugerenciaFragment()).commit();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }

            @Override
            public void onFragmentInteraction(Uri uri) {
            }



            public void AgregarServicio_Click(View view) throws IOException {
                //region temporal
                Random r = new Random();
                int low = 1000;
                int high = 2000;
                int result = r.nextInt(high-low) + low;
                //endregion temporal
                int pkIdService = result;
                EditText serviceName = (EditText) findViewById(R.id.txtServicio);
                Spinner serviceType = (Spinner) findViewById(R.id.spinnerTipoServicio);
                EditText serviceDescription = (EditText) findViewById(R.id.txtDescripcion);
                EditText serviceCreation = (EditText) findViewById(R.id.txtFecha);
                EditText serviceEquipment = (EditText) findViewById(R.id.txtEquipamiento);
                String serviceStart = "";
                String serviceEnd = "";
                String idClient = "";
                String clientContract = "";
                serviceEquipment.setText("false");

                //region SPLIT
                String nrecog = serviceName.getText().toString();
                String[] arrayserv = nrecog.split("descripción");
                serviceName.setText(arrayserv[0].toString());
                Log.i("LOGALF===>" ,""+arrayserv.length);
                if (arrayserv.length>1){
                    serviceDescription.setText(arrayserv[1].toString());
                }else {
                    serviceDescription.setText(serviceName.getText().toString());
                }

                //endregion SPLIT

                //region POST TO API
                String json = "{\"pkIdService\":"+pkIdService+","
                        + "\"serviceName\":\""+serviceName.getText().toString()+"\","
                        + "\"serviceType\":\""+serviceType.getSelectedItem().toString()+"\","
                        + "\"serviceDescription\":\""+serviceDescription.getText().toString()+"\","
                        + "\"serviceCreation\":\""+serviceCreation.getText().toString()+"\","
                        + "\"serviceEquipment\":"+serviceEquipment.getText().toString()+","
                        + "\"serviceStart\":\""+"\","
                        + "\"serviceEnd\":\""+"\","
                        + "\"idClient\":\""+"\","
                        + "\"clientContract\":\""+"IMPXX"+"\""
                        + "}";
                Log.i("LOGALF====>" ,json.toString());
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            throw new IOException("Unexpected code " + response);
                        } else {
                            String cadenaJson = response.body().string();
                            Log.i("====>", cadenaJson);

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast toast= Toast.makeText(getApplicationContext(), "Se agrego servicio", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                    fragmentManager.beginTransaction().replace(R.id.contenedor, new ServiceFragment()).commit();
                                    Limpiar();
                                }
                            });

                        }
                    }
                });
                //endregion POST TO API
            }


            public void Limpiar(){
                EditText amount = (EditText) findViewById(R.id.txtEquipamiento);
                EditText date_expense = (EditText) findViewById(R.id.txtFecha);
                EditText description = (EditText) findViewById(R.id.txtDescripcion);
                EditText local = (EditText) findViewById(R.id.txtServicio);
                Spinner category = (Spinner) findViewById(R.id.spinnerTipoServicio);

                amount.setText("");
                date_expense.setText("");
                description.setText("");
                local.setText("");
                category.setSelection(1);
            }

            //region fragment_service : Azure Cognitive Service
            @SuppressLint("SetTextI18n")
            public void escucharServicio_Click(View view) {
                TextView txtrecog1 =(TextView) this.findViewById(R.id.txtServicio);
                TextView txtrecog2 =(TextView) this.findViewById(R.id.txtDescripcion);
                final ProgressBar pb = (ProgressBar) this.findViewById(R.id.progressbar);
                //pb.setVisibility(View.VISIBLE);
               /* new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pb.setVisibility(View.VISIBLE);
                    }
                }, 1000);*/
                try{
                    SpeechConfig config = SpeechConfig.fromSubscription(speechSubscriptionKey,
                            serviceRegion);
                    config.setSpeechRecognitionLanguage(LanguageRecognition);
                    assert(config != null);
                    com.microsoft.cognitiveservices.speech.SpeechRecognizer reco = new
                            com.microsoft.cognitiveservices.speech.SpeechRecognizer(config);
                    assert(reco != null);
                    Future<SpeechRecognitionResult> task = reco.recognizeOnceAsync();
                    assert(task != null);
                    SpeechRecognitionResult result = task.get();
                    //result.wait(6000);
                    assert(result != null);
                    if (result.getReason() == ResultReason.RecognizedSpeech) {
                        Log.i("LOGALF====>", "Llegamos Result Speech");
                        txtrecog1.setText(result.getText());
                        Toast.makeText(this,result.getDuration().toString()+"|"+result.getProperties(),Toast.LENGTH_SHORT).show();
                    }
                    else {
                        txtrecog2.setText("Error recognizing. Did you update the subscription info?" + System.lineSeparator() + result.toString());
                    }
                    reco.close();
                }catch (Exception ex){
                    Log.e("SpeechSDKDemo", "unexpected " + ex.getMessage());
                    assert(false);
                }
            }
            //endregion


            //region Funcionalidades SQLite Sugerencias

            public void btnAgregarSugerencia(View view) {
                EditText titulo = (EditText) findViewById(R.id.titulo);
                EditText descripcion = (EditText) findViewById(R.id.descripcion);

                SugerenciaDAO dao = new SugerenciaDAO(getBaseContext());
                try {
                    //dao.eliminarTodos();
                    dao.insertar(titulo.getText().toString(), descripcion.getText().toString());

                    Toast toast= Toast.makeText(getApplicationContext(), "Se insertó correctamente", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();

                    titulo.setText("");
                    descripcion.setText("");
                } catch (DAOException e) {
                    Log.i("GeneroMusicalNuevoActi", "====> " + e.getMessage());
                }
                //Toast.makeText(this,"Se agrego Nuevo",Toast.LENGTH_SHORT).show();
            }

            public void ListSugerencia_OnClick(View view) {
                EditText criterio = (EditText) findViewById(R.id.criterio);
                SugerenciaDAO dao = new SugerenciaDAO(getBaseContext());
                try {
                    ArrayList<Sugerencia> resultados = dao.buscar(criterio.getText().toString());

                    String[] encontrados = new String[resultados.size()];
                    int i = 0;
                    for (Sugerencia gm : resultados){
                        encontrados[i++] = gm.getTitulo();
                    }


                    ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this.getApplicationContext(),
                            android.R.layout.simple_list_item_1,
                            encontrados);

                    ListView listaResultados = (ListView)findViewById(R.id.listaResultados);
                    listaResultados.setAdapter(adaptador);


                } catch (DAOException e) {
                    Log.i("GeneroMusicalBuscarAc", "====> " + e.getMessage());
                }
            }

            //endregion
        }
