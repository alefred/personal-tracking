package com.example.personaltracking.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.personaltracking.Entity.Gasto;
import com.example.personaltracking.R;
import java.util.List;

public class GastoAdapter extends RecyclerView.Adapter<GastoAdapter.MiHolder> {

    private List<Gasto> gastoList;

    @NonNull
    @Override
    public MiHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.service_fila, viewGroup, false);
        return new MiHolder(itemView);
    }

    public class MiHolder extends RecyclerView.ViewHolder {
        public TextView descrip,monto,local,categoria,fecha;
        public MiHolder(@NonNull View itemView) {
            super(itemView);
            descrip=(TextView) itemView.findViewById(R.id.descripcion);
            monto=(TextView) itemView.findViewById(R.id.servicioEstado);
            local=(TextView) itemView.findViewById(R.id.local);
            categoria=(TextView) itemView.findViewById(R.id.categoria);
            fecha=(TextView)  itemView.findViewById(R.id.fecha);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MiHolder miHolder, int i) {
        Gasto gasto = gastoList.get(i);
        miHolder.descrip.setText(gasto.getDescripcion());
        miHolder.monto.setText(gasto.getMonto());
        miHolder.local.setText(gasto.getLocal());
        miHolder.categoria.setText(gasto.getCategoria());
        miHolder.fecha.setText(gasto.getFecha());
    }

    @Override
    public int getItemCount() {
        return gastoList.size();
    }



    public GastoAdapter(List<Gasto> gastoList){
        this.gastoList = gastoList;
    }
}
