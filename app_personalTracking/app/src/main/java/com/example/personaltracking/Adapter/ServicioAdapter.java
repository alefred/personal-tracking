package com.example.personaltracking.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.personaltracking.Entity.Servicio;
import com.example.personaltracking.R;

import java.util.List;

public class ServicioAdapter extends  RecyclerView.Adapter<ServicioAdapter.MiHolder> {
    private List<Servicio> servicioList;

    public ServicioAdapter(List<Servicio> servicioList){
        this.servicioList = servicioList;
    }

    @NonNull
    @Override
    public MiHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.service_fila, viewGroup, false); //TODO: MODIFICAR GASTOFILA
        return new MiHolder(itemView);
    }

    public class MiHolder extends RecyclerView.ViewHolder {
        public TextView descrip,monto,local,categoria,fecha;
        public MiHolder(@NonNull View itemView) {
            super(itemView);
            descrip=(TextView) itemView.findViewById(R.id.descripcion);
            monto=(TextView) itemView.findViewById(R.id.servicioEstado);
            local=(TextView) itemView.findViewById(R.id.local);
            categoria=(TextView) itemView.findViewById(R.id.categoria);
            fecha=(TextView)  itemView.findViewById(R.id.fecha);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MiHolder miHolder, int i) {
        Servicio servicio = servicioList.get(i);
        miHolder.descrip.setText(servicio.getServiceDescription());
        miHolder.monto.setText("Pendiente"); //TODO: Cambiat por el estado del servicio
        miHolder.local.setText(servicio.getServiceName());
        miHolder.categoria.setText(servicio.getServiceType());
        miHolder.fecha.setText(servicio.getServiceCreation());
    }

    @Override
    public int getItemCount() {
        return servicioList.size();
    }


}
