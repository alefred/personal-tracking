package com.example.personaltracking.Entity;

public class GastoMarcador {
    private String titulo;
    private String snippet;
    private Double lat;
    private Double lng;

    public GastoMarcador(String titulo, String snippet, Double lat, Double lng) {
        this.titulo = titulo;
        this.snippet = snippet;
        this.lat = lat;
        this.lng = lng;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
