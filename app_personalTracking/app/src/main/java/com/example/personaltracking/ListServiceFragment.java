package com.example.personaltracking;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.personaltracking.Adapter.ServicioAdapter;
import com.example.personaltracking.Entity.Servicio;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListServiceFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListServiceFragment extends Fragment {
    private ServicioAdapter miAdapter;
    private List<Servicio> servicioList=new ArrayList<>();

    private OnFragmentInteractionListener mListener;

    public ListServiceFragment() {
        // Required empty public constructor
    }

    public static ListServiceFragment newInstance(String param1, String param2) {
        ListServiceFragment fragment = new ListServiceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_list_service, container, false);
        final RecyclerView mirecycler = (RecyclerView) rootView.findViewById(R.id.elreciclador);
        OkHttpClient client = new OkHttpClient();

        miAdapter= new ServicioAdapter(servicioList);
        RecyclerView.LayoutManager miLayoutManager = new LinearLayoutManager(getActivity());
        mirecycler.setLayoutManager(miLayoutManager);
        mirecycler.setItemAnimator(new DefaultItemAnimator());
        mirecycler.setAdapter(miAdapter);

        Log.i("LOGALF====>", "Llegamos antes de region rest");

        Request request = new Request.Builder()
                .url("http://wspersonaltrackingservice.azurewebsites.net/api/service")
                .build();
        //solo se crea el get

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    String cadenaJson = response.body().string();
                    Log.i("LOGALF====>", "Consumimos el Servicio response"+ cadenaJson);

                    servicioList.clear();
                    Servicio servicio;
                    Gson gson = new Gson();
                    Type stringStringMap = new TypeToken<ArrayList<Map<String, Object>>>() { }.getType();
                    final ArrayList<Map<String, Object>> retorno = gson.fromJson(cadenaJson, stringStringMap);

                    for (Map<String, Object> x : retorno) {//Integer.parseInt() /Boolean.parseBoolean()
                        String xservicestart="";
                        String xserviceend="";
                        if (x.get("serviceStart")!=null){
                            xservicestart =x.get("serviceStart").toString();
                        }
                        if (x.get("serviceStart")!=null){
                            xserviceend = x.get("serviceEnd").toString();
                        }
                        servicio = new Servicio(x.get("pkIdService").toString(),x.get("serviceName").toString(),
                                x.get("serviceType").toString(), x.get("serviceDescription").toString(),
                                x.get("serviceCreation").toString(), x.get("serviceEquipment").toString(),
                                xservicestart, xserviceend,
                                x.get("idClient").toString(), x.get("clientContract").toString());
                        servicioList.add(servicio);
                        Log.i("LOGALF====>", "SERVICIO: "+ servicio.getServiceDescription());
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            miAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

        return rootView;
    }

    //TODO: BORRAR al terminar
    /*private void prepararDatosGasto() {
        Gasto gasto=new Gasto("UPC FOODCURT","Snacktarde","5.5","COMIDA","02/04/2019");
        gastoList.add(gasto);
        gasto=new Gasto("Bembos San Isidro","Almuerzo","12.5","COMIDA","01/04/2019");
        gastoList.add(gasto);
        gasto=new Gasto("Cineplanet","Entradas Avengers EndGame","37.60","CINE","24/04/2019");
        gastoList.add(gasto);
    }*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
