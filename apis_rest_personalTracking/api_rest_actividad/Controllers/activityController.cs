﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest_actividad.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace api_rest_actividad.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class activityController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            using (arsDB05Context _context = new arsDB05Context())
            {
                var actividades = _context.Activities;
                return Ok(actividades.ToList());
            }
        }

        [HttpPost]
        public IActionResult Post(JObject payload)
        {
            using (arsDB05Context _context = new arsDB05Context())
            {
                Activities actividad = payload.ToObject<Activities>();
                _context.Activities.Add(actividad);
                _context.SaveChanges();
                return Ok("Actividad Guardado MOF!");
            }
        }
    }
}