﻿using System;
using System.Collections.Generic;

namespace api_rest_actividad.Models
{
    public partial class Activities
    {
        public int PkIdActivity { get; set; }
        public string ActivityType { get; set; }
        public int? ActivityDuration { get; set; }
        public string ActivityDescription { get; set; }
        public DateTime? ActivityStart { get; set; }
        public DateTime? ActivityEnd { get; set; }
        public int? FkIdService { get; set; }

        public virtual Services FkIdServiceNavigation { get; set; }
    }
}
