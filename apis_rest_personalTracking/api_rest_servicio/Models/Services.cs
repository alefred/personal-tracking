﻿using System;
using System.Collections.Generic;

namespace api_rest_servicio.Models
{
    public partial class Services
    {
        public Services()
        {
            Activities = new HashSet<Activities>();
        }

        public int PkIdService { get; set; }
        public string ServiceName { get; set; }
        public string ServiceType { get; set; }
        public string ServiceDescription { get; set; }
        public DateTime? ServiceCreation { get; set; }
        public bool? ServiceEquipment { get; set; }
        public DateTime? ServiceStart { get; set; }
        public DateTime? ServiceEnd { get; set; }
        public string IdClient { get; set; }
        public string ClientContract { get; set; }

        public virtual ICollection<Activities> Activities { get; set; }
    }
}
