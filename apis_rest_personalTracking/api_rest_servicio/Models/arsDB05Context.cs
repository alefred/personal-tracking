﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace api_rest_servicio.Models
{
    public partial class arsDB05Context : DbContext
    {
        public arsDB05Context()
        {
        }

        public arsDB05Context(DbContextOptions<arsDB05Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Activities> Activities { get; set; }
        public virtual DbSet<Services> Services { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("azDb1"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activities>(entity =>
            {
                entity.HasKey(e => e.PkIdActivity)
                    .HasName("PK__Activiti__276ECA17CD78081A");

                entity.Property(e => e.PkIdActivity)
                    .HasColumnName("pk_id_activity")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivityDescription)
                    .HasColumnName("activity_description")
                    .IsUnicode(false);

                entity.Property(e => e.ActivityDuration).HasColumnName("activity_duration");

                entity.Property(e => e.ActivityEnd)
                    .HasColumnName("activity_end")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActivityStart)
                    .HasColumnName("activity_start")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActivityType)
                    .HasColumnName("activity_type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FkIdService).HasColumnName("fk_id_service");

                entity.HasOne(d => d.FkIdServiceNavigation)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.FkIdService)
                    .HasConstraintName("FK__Activitie__fk_id__6FE99F9F");
            });

            modelBuilder.Entity<Services>(entity =>
            {
                entity.HasKey(e => e.PkIdService)
                    .HasName("PK__Services__ABE19D9B7977DA20");

                entity.Property(e => e.PkIdService)
                    .HasColumnName("pk_id_service")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientContract)
                    .HasColumnName("client_contract")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdClient)
                    .HasColumnName("id_client")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceCreation)
                    .HasColumnName("service_creation")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceDescription)
                    .HasColumnName("service_description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceEnd)
                    .HasColumnName("service_end")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceEquipment).HasColumnName("service_equipment");

                entity.Property(e => e.ServiceName)
                    .HasColumnName("service_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceStart)
                    .HasColumnName("service_start")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceType)
                    .HasColumnName("service_type")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
