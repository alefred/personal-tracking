﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using api_rest_servicio.Models;

namespace api_rest_servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class serviceController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get() {
            using (arsDB05Context _context = new arsDB05Context()) {
                var servicios = _context.Services;
                return Ok(servicios.ToList());
            }
        }

        [HttpPost]
        public IActionResult Post(JObject payload) {
            using (arsDB05Context _context = new arsDB05Context()) {
                Services servicio = payload.ToObject<Services>();
                _context.Services.Add(servicio);
                _context.SaveChanges();
                return Ok("Servicio Guardado MOF!");
            }
        }
    }
}