--ARSDB03
SELECT * from Logs
insert into Logs values('Prueba Web api')


--ARSDB05
drop table Pubs
drop table Activities
drop table Services


-- TABLAS PP2
CREATE TABLE Services(
    pk_id_service INT NOT NULL,
    service_name VARCHAR(50),
    service_type VARCHAR(20),
    service_description VARCHAR(200),
    service_creation DATETIME,
    service_equipment BIT,
    service_start DATETIME,
    service_end DATETIME,
    id_client VARCHAR(20),
    client_contract VARCHAR(50),
    PRIMARY KEY (pk_id_service)
)

CREATE TABLE Activities(
    pk_id_activity INT NOT NULL,
    activity_type VARCHAR (20),
    activity_duration INT ,
    activity_description VARCHAR (MAX),
    activity_start DATETIME,
    activity_end DATETIME,
    fk_id_service INT,
    PRIMARY KEY (pk_id_activity)
)

ALTER TABLE Activities 
ADD FOREIGN KEY (fk_id_service) REFERENCES Services(pk_id_service)

Insert into Services values (100,'Soporte Datacenter', 'Infraestructura',
'El cliente necesita configurar 1 CISCO NEXUS CX1 y Creacion de un cluster de Servidores Vmware',
'20120618 10:34:09 AM', 0,'20130618 10:34:09 AM', '20140618 10:34:09 AM',100,'IMPRG100')
Insert into Activities values (102,'Inicio-Recoleccion','2','entrevista con el cliente para verificar el trabajo en el sitio',
'20130618 10:34:09 AM', '20140618 11:34:09 AM',100)

select * from Services
select * from Activities
--OTROS:
--pk_id_service INT NOT NULL IDENTITY(1,1),
