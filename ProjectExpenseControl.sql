--Tabla WS Gastos
CREATE TABLE [dbo].[t_expenses](
	[id_expense] [int] NOT NULL,
	[local] [varchar](50) NULL,
	[description] [varchar](50) NULL,
	[amount] [decimal](18, 2) NULL,
	[category] [varchar](50) NULL,
	[date_expense] [datetime] NULL
) ON [PRIMARY]

--Tabla WS Imagenes (Imagenes de hasta 2GB) - Facturas
CREATE TABLE [dbo].[t_expenseInvoices](
	[id_expense_invoice] [int] NOT NULL,
	[id_expense] [int] NOT NULL,
	[image] [VARCHAR](max) NULL,
	[note] [varchar](50) NULL,
) ON [PRIMARY]


drop table [t_expenses]

select * from t_expenses
select * from t_expenseInvoices