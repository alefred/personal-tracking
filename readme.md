# App para Seguimiento de Personal  🚀
## Descripción  📋

Esta aplicacion android ha sido desarrollada como una muestra PoC para el proyecto integrador de la carrera de ingenieria de sistemas, la aplicación realiza un registro de servicio mediante voz hace el split de los datos y los persiste mediante apis desplegadas en Azure Apps Service

ASP Net Core + Entity Framwork core + Android SDk 27 + Speech to Text SDK.🛠️


## Uso de Web Services ⌨️  

### API Servicio:
* GET: https://wspersonaltrackingservice.azurewebsites.net/api/service
+ GET: https://wspersonaltrackingservice.azurewebsites.net/api/service
- POST: https://wspersonaltrackingservice.azurewebsites.net/api/service

Body:
```
{
    "pkIdService": 200,
    "serviceName": "Consultoria Seguridad",
    "serviceType": "Seguridad",
    "serviceDescription": "Ethical Hacking a aplicaciones web",
    "serviceCreation": "2013-06-18T10:34:09",
    "serviceEquipment": false,
    "serviceStart": "2014-06-18T10:34:09",
    "serviceEnd": "2015-06-18T10:34:09",
    "idClient": "200",
    "clientContract": "IMPRG200"
}
```

### API Actividades
* GET: https://wspersonaltrackingactivity.azurewebsites.net/api/activity
+ POST: https://wspersonaltrackingactivity.azurewebsites.net/api/activity

Body:
```
{
    "pkIdActivity": 201,
    "activityType": "Ejecucion",
    "activityDuration": 4,
    "activityDescription": "Analisis de vulnerabilidades con owasp zap",
    "activityStart": "2013-06-18T10:34:09",
    "activityEnd": "2014-06-18T11:34:09",
    "fkIdService": 200,
}
```

## Base de Datos: 📌
Ambos se encuentran en la suscripcion IC de Azure.

* SRV: azrdb05.database.windows.net
+ DB: arsDB05

##License  📄
[MIT](https://choosealicense.com/licenses/mit/)

>Copyright (c) [2020] [Alfredo Arias @alefred]

**Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.**
